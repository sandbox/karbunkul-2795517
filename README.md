# Developer Tools for Drupal 8

## Features

- Convert path to route
- Information about extensions
- Toolbar integration
- Flush caches
- Test page callbacks
- Code benchmark

## Install development dependencies

- `npm install`
- `gulp watch`

## Credits

- [Alexander Pokhodyun](https://vk.com/karbunkul) - Developer

Copyright &copy; 2016 Alexander Pokhodyun
