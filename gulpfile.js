/**
 * Created by karbunkul on 14/08/15.
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var notify = require("gulp-notify");

var srcSCSS = 'scss';
var outCSS = 'css';

/* Задачи по умолчанию при вызове Gulp */
gulp.task('default', ['watch', 'sass']);

/* Генерация css из scss */
gulp.task('sass', function () {
  gulp.src(srcSCSS + '/*.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest(outCSS))
    .pipe(notify("Sass task is done"))
});

// Отслеживание изменений в файлах
gulp.task('watch', function () {
  gulp.watch(srcSCSS + '/*.scss', ['sass']);
});