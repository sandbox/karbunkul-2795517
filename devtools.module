<?php

/**
 * @file
 * Contains devtools.module.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\devtools\CodeGenDefinition;

/**
 * Implements hook_code_generator().
 */
function devtools_code_generator() {
  $items = [];

  $definition = new CodeGenDefinition();
  $definition->setId('module');
  $definition->setTitle('Create new module');
  $definition->setDescription('Generate new module template');
  $definition->setForm('\Drupal\devtools\Form\CodeGeneratorCreateNewModule');

  $items[] = $definition;

  $definition = new CodeGenDefinition();
  $definition->setId('theme');
  $definition->setTitle('Create new theme');
  $definition->setDescription('Generate new theme template');
  $definition->setForm('\Drupal\devtools\Form\CodeGeneratorContext');

  $items[] = $definition;

  $definition = new CodeGenDefinition();
  $definition->setId('block');
  $definition->setTitle('Add block');
  $definition->setDescription('Add block for context module');
  $definition->setForm('\Drupal\devtools\Form\CodeGeneratorCreateNewModule');
  $definition->setContext($definition::CONTEXT_MODULE);

  $items[] = $definition;

  return $items;
}

/**
 * Implements hook_form_alter().
 */
function devtools_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ((\Drupal::currentUser()->hasPermission('access site devtools'))) {
    $buildInfo = $form_state->getBuildInfo();
    $form['form_build_info'] = [
      '#markup' => new FormattableMarkup('<div class="form-build-info messages messages--warning"><div>Form ID - @id</div>  <div>Namespace - \Drupal::formBuilder()
      ->getForm(\'@callback\');</div></div>', [
        '@callback' => get_class($buildInfo['callback_object']),
        '@id' => $form_id,
      ]),
      '#weight' => -150,
    ];
  }

  $content_types = \Drupal::entityTypeManager()
                          ->getStorage('node_type')
                          ->loadMultiple();
  foreach ($content_types as $name => $obj) {
    if (('node_' . $name . '_form' == $form_id) || ('node_' . $name . '_edit_form' == $form_id)) {
      // узнаем есть ли у пользователя права
      if (\Drupal::currentUser()->hasPermission('access site devtools')) {
        // Добавляем секцию
        $form['devtools'] = [
          '#type' => 'details',
          '#title' => 'Developer Tools',
          '#group' => 'advanced',
          '#weight' => -100,
        ];

        // Выводим машинное имя
        $form['devtools']['machine_name'] = [
          '#butt' => 'markup',
          '#markup' => new FormattableMarkup('<h4 class="label inline">@title</h4>  - @mn', [
            '@mn' => $name,
            '@title' => t('Machine name'),
          ]),
          '#group' => 'devtools',
        ];

        // Создаем нужные ссылки
        $commonAttributes = ['attributes' => array('target' => '_blank')];
        $editUrl = \Drupal\Core\Url::fromRoute('entity.node_type.edit_form', ['node_type' => $name], $commonAttributes);
        $manageFieldsUrl = \Drupal\Core\Url::fromRoute('entity.node.field_ui_fields', ['node_type' => $name], $commonAttributes);
        $manageDisplayUrl = \Drupal\Core\Url::fromRoute('entity.entity_view_display.node.default', ['node_type' => $name], $commonAttributes);

        // Выводим ссылки
        $form['devtools']['lin'] = array(
          '#theme' => 'item_list',
          '#title' => t('Operations'),
          '#items' => array(
            \Drupal::l(t('Edit content type'), $editUrl),
            \Drupal::l(t('Manage fields'), $manageFieldsUrl),
            \Drupal::l(t('Manage display'), $manageDisplayUrl),
          ),
        );
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function devtools_theme($existing, $type, $theme, $path) {
  $theme_item['devtools_module_information'] = [
    'render element' => 'elements',
    'variables' => [
      'title' => NULL,
      'example' => NULL,
      'value' => NULL,
    ],
  ];

  $theme_item['devtools_generator_information'] = [
    'render element' => 'elements',
    'variables' => [
      'title' => NULL,
      'description' => NULL,
      'link' => NULL,
    ],
  ];

  return $theme_item;
}

/**
 * Implements hook_toolbar().
 */
function devtools_toolbar() {
  $routeParameters = ['js' => 'nojs'];
  $options = [
    'attributes' => [
      'class' => ['use-ajax'],
      'data-accepts' => 'application/vnd.drupal-modal',
    ],
  ];

  $routeModalUrl = \Drupal\Core\Url::fromRoute('devtools.modal_path_to_route', $routeParameters, $options);

  $items['devtools'] = [
    '#type' => 'toolbar_item',
    'tab' => [
      '#type' => 'link',
      '#title' => t('Developer Tools'),
      '#url' => Url::fromRoute('devtools.admin_devtools'),
      '#attributes' => [
        'title' => t('Developer Tools'),
        'class' => ['toolbar-icon', 'toolbar-icon-devtools'],
      ],
    ],
    'tray' => array(
      '#heading' => t('Developer Tools'),
      'devtools_links' => array(
        '#theme' => 'links__toolbar_devtools',
        '#attributes' => ['class' => ['toolbar-menu toolbar-menu--devtools']],
        '#links' => array(

          'flush_caches' => [
            'title' => t('Cache clear'),
            'url' => Url::fromRoute('devtools.flush_caches'),
            'attributes' => [
              'title' => t('Flush all caches'),
              'class' => ['toolbar-icon', 'toolbar-icon--flush'],
            ],
          ],
          'path' => [
            'title' => t('Path to Route'),
            'url' => $routeModalUrl,
            'attributes' => [
              'title' => t('Path to Route'),
              'class' => ['toolbar-icon', 'toolbar-icon--path-to-route'],
            ],
          ],
          'extension' => [
            'title' => t('Extension info'),
            'url' => Url::fromRoute('devtools.modules'),
            'attributes' => [
              'title' => t('Modules information'),
              'class' => ['toolbar-icon', 'toolbar-icon--extension-info'],
            ],
          ],
          'codegen' => [
            'title' => t('Code generator'),
            'url' => Url::fromRoute('devtools.codegen'),
            'attributes' => [
              'title' => t('Code generator'),
              'class' => ['toolbar-icon', 'toolbar-icon--code-generator'],
            ],
          ],
          'forminfo' => [
            'title' => t('Form information'),
            'url' => Url::fromRoute('devtools.form_build_info', ['js' => 'nojs']),
            'attributes' => [
              'title' => t('Modules information'),
              'class' => ['use-ajax', 'toolbar-icon', 'toolbar-icon--form-info'],
            ],
          ],


        ),
      ),
    ),
    '#attached' => [
      'library' => [
        'core/drupal.dialog.ajax',
      ],
    ],
  ];

  return $items;
}

/**
 * Implements hook_preprocess().
 */
function devtools_preprocess(&$variables, $hook) {

}

/**
 * Implements hook_preprocess_HOOK().
 */
function devtools_preprocess_html(&$variables) {

//  krumo($variables['html_attributes']->toArray());

  if (isset($variables['html_attributes']) && (\Drupal::currentUser()
                                                      ->hasPermission('access site devtools'))
  ) {
    $variables['html_attributes']->addClass('devtools');
    $showFormBuildInfo = \Drupal::config('devtools.settings')
                               ->get('show_form_buid_info');
    if ($showFormBuildInfo) {
      $variables['html_attributes']->addClass('form-build-info');
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function devtools_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'devtools/devtools.global';
  $attachments['#attached']['library'][] = 'devtools/devtools.toolbar_icons';
}
