<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 15/08/15
 * Time: 22:41
 */

namespace Drupal\devtools;

class CodeGen {

  public static function getGenerator($id) {
    $generator = new CodeGenDefinition();
    $generator = (isset(self::getGenerators()[$id])) ? self::getGenerators()[$id] : FALSE;
    return $generator;
  }

  public static function getGenerators() {
    $hookItems = \Drupal::moduleHandler()->invokeAll('code_generator');
    $result = [];
    foreach ($hookItems as $definition) {
      if ($definition->isValidate()) {
        $result[$definition->getId()] = $definition;
      }
    }
    return $result;
  }


  public static function setSettingsSubmitForm($form, $title = 'Generate') {

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t($title),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public static function setStorageContext($id, $type, $data) {
    \Drupal::cache()->set(self::getContextCid($id, $type), $data);
  }

  private static function getContextCid($id, $type) {
    return $id . ':' . $type . ':' . \Drupal::currentUser()
      ->getAccount()
      ->id();
  }

  public static function getStorageContext($id, $type) {
    return \Drupal::cache()->get(self::getContextCid($id, $type))->data;
  }

}

