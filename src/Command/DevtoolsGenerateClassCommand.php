<?php

/**
 * @file
 * Contains \Drupal\devtools\Command\DevtoolsGenerateClassCommand.
 */

namespace Drupal\devtools\Command;

use Drupal\Console\Command\ConfirmationTrait;
use Drupal\Console\Command\GeneratorCommand;
use Drupal\Console\Command\ModuleTrait;
use Drupal\Console\Style\DrupalStyle;
use Symfony\Component\Console\Input;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DevtoolsGenerateClassCommand.
 *
 * @package Drupal\devtools
 */
class DevtoolsGenerateClassCommand extends GeneratorCommand {

  use ModuleTrait;
  use ConfirmationTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('devtools:generate:class')
      ->setDescription($this->trans('command.devtools.generate.class.description'))
      ->addOption('module', '', InputOption::VALUE_REQUIRED, $this->trans('commands.common.options.module'))
      ->addOption('class', '', InputOption::VALUE_REQUIRED, $this->trans('command.devtools.generate.class.options.class'))
      ->addOption('fields', '', InputOption::VALUE_REQUIRED, $this->trans('command.devtools.generate.class.options.fields'));
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $module = $input->getOption('module');
    $class = $input->getOption('class');

    $option_fields = $input->getOption('fields');

    $fields = [];
    foreach (explode(' ', $option_fields) as $name) {
      $under_score = preg_replace('/_/', ' ', $name);
      $field = [
        'machine_name' => $name,
        'camel_case' => preg_replace('/ /', '', ucwords($under_score)),
      ];
      $fields[] = (object) $field;
    }

    $yes = $input->hasOption('yes')?$input->getOption('yes'):false;
    if (!$this->confirmGeneration($io, $yes)) {
      return;
    }

    $io->info($class);

    $this->getGenerator()->generate($module, $class, $fields);
  }

  protected function interact(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $module = $input->getOption('module');
    if (!$module) {
      // @see Drupal\Console\Command\ModuleTrait::moduleQuestion
      $module = $this->moduleQuestion($output);
      $input->setOption('module', $module);
    }

    $class = $input->getOption('class');
    if (!$class) {
      $class = $io->ask($this->trans('command.devtools.generate.class.options.class_desc'), 'ClassName');
      $input->setOption('class', $class);
    }

    $fields = $input->getOption('fields');
    if (!$fields) {
      $fields = $io->ask($this->trans('command.devtools.generate.class.options.fields_desc'), 'id title access_callback');
      $input->setOption('fields', $fields);
    }
  }

  protected function createGenerator() {
    return new DevtoolsGenerateClassGenerator();
  }
}
