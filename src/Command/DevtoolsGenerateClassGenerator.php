<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 19.03.16
 * Time: 11:19
 */

namespace Drupal\devtools\Command;

use Drupal\Console\Generator\Generator;

class DevtoolsGenerateClassGenerator extends Generator{

  public function generate($module, $class, $fields) {
    $template = 'devtools-class-generator.html.twig';

    $parameters = [
      'module' => $module,
      'class' => $class,
      'fields' => $fields,
    ];

    $target = $this->getSite()->getModulePath($module) . '/src/' . $class . '.php';

    $this->renderFile(
      $template,
      $target,
      $parameters
    );

  }

}