<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 17/08/15
 * Time: 00:42
 */

namespace Drupal\devtools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devtools\CodeGen;

class CodeGeneratorCreateNewModule extends FormBase {
  public function getFormId() {
    return 'devtools_code_generator_create_new_module_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['name']['label'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Module name'),
        '#required' => TRUE,
        '#size' => 32,
        '#default_value' => '',
        '#maxlength' => 255,
    );
    $form['name']['id'] = array(
        '#type' => 'machine_name',
        '#maxlength' => 128,
        '#machine_name' => array(
            'exists' => '\Drupal\views\Views::getView',
            'source' => array('name', 'label'),
        ),
        '#description' => $this->t('A unique machine-readable name for this View. It must only contain lowercase letters, numbers, and underscores.'),
    );

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => 'Description',
      '#resizable' => FALSE,
      '#required' => TRUE,
    ];

    $form['version'] = [
      '#type' => 'textfield',
      '#title' => 'Version',
      '#size' => 55,
      '#field_prefix' => '8.x-',
      '#default_value' => '1.0'
    ];

    $form['package'] = [
      '#type' => 'textfield',
      '#title' => 'Package',
    ];

    $form = CodeGen::setSettingsSubmitForm($form);

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }
}