<?php

namespace Drupal\devtools\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devtools\CodeBenchmark;
use Drupal\devtools\Plugin\CodeBenchmarkBase;

/**
 * Class CodeBenchmarkCallForm.
 *
 * @package Drupal\devtools\Form
 */
class CodeBenchmarkCallForm extends ConfigFormBase {

  protected $benchmark;
  protected $benchmarkId;
  protected $benchmarkConfig;

  const BENCHMARK_CONFIG = 'devtools.code_benchmark';

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $id = $this->getRequest()->get('id');

    if ($benchmark = CodeBenchmark::getBenchmarkById($id)) {
      $this->setBenchmark($benchmark);
      $this->benchmarkConfig = $this->config('devtools.code_benchmark');
      $this->benchmarkId = $id;
    }
    else {
      drupal_set_message($this->t('Benchmark "@id" not found', ['@id' => $id]), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devtools_code_benchmark_call_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->getBenchmark()) {
      return $form;
    }

    if ($results = $this->config(self::BENCHMARK_CONFIG)->get($this->benchmarkId)) {
      //dsm($results);

      $template = '<h1>{{ name }}</h1>';
      $template.= '<ul>{% for key, result in results %}<li>{{ key }} - {{ result.average }}</li>{% endfor %}</ul>';
      $template.= '<hr>';
      $template.= '<h3>{{ function }} - {{ average }}</h3>';

      $form['results'] = [
        '#type' => 'inline_template',
        '#template' => $template,
        '#weight' => -100,
        '#context' => [
          'name' => $this->getBenchmark()->getLabel(),
          'results' => $results['results'],
          'function' => $results['function'],
          'average' => $results['average'],
        ],
      ];
    }

    $config = $this->config('devtools.settings');

    $form['count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of passes'),
      '#default_value' => $config->get('number_of_passes'),
      '#required' => TRUE,
      '#attributes' => [
        'data-type' => 'number',
      ],
      '#maxlength' => 3,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Start benchmark'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    CodeBenchmark::newInstance($this->getBenchmark(), $form_state->getValue('count'));
    $this->config('devtools.settings')
      ->set('number_of_passes', $form_state->getValue('count'))
      ->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      'devtools.code_benchmark',
      'devtools.settings',
    ];
  }

  /**
   * Get benchmark instance.
   *
   * @return CodeBenchmarkBase
   *   Benchmark instance.
   */
  private function getBenchmark() {
    return $this->benchmark;
  }

  /**
   * Set benchmark instance.
   *
   * @param CodeBenchmarkBase $benchmark
   *   Benchmark instance.
   */
  private function setBenchmark(CodeBenchmarkBase $benchmark) {
    $this->benchmark = $benchmark;
  }

  /**
   * @return \Drupal\Core\Config\Config
   */
  public function getBenchmarkConfig() {
    return $this->benchmarkConfig;
  }

  /**
   * @param \Drupal\Core\Config\Config
   */
  public function setBenchmarkConfig($benchmarkConfig) {
    $this->benchmarkConfig = $benchmarkConfig;
  }



}
