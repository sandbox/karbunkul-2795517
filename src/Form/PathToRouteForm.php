<?php
/**
 * @file
 */

namespace Drupal\devtools\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class PathToRouteForm extends FormBase {
  public function getFormId() {
    return 'devtools_path_to_route_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $current = Url::fromRouteMatch(\Drupal::routeMatch());

    if ($refer = \Drupal::request()->headers->get('referer')) {
      $refer = str_replace(\Drupal::request()
        ->getSchemeAndHttpHost(), '', $refer);
    }

    $path = $form_state->getValue('path', FALSE);

    if ($current->getRouteName() != 'devtools.url') {
      $path = $refer;
    }

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => 'Path',
      '#description' => 'Input url path',
      '#required' => TRUE,
      '#default_value' => $path,
      '#element_validate' => array(
        array($this, 'validatePath'),
      ),
      '#attributes' => array(
        'placeholder' => 'For example /node/add'
      ),
    );

    $form['actions'] = array('#type' => 'actions');

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Get Route object'),
      '#button_type' => 'primary',
      '#ajax' => array(
        'callback' => array($this, 'ajaxUrl'),
      ),
    );

    $message = '';

    if ($path) {
      $urlRoute = Url::fromUserInput($path);
      if ($urlRoute->getRouteName()) {
        $message = format_string('<div class="messages messages--status">$routeParameters = @parameters; <br />$options = @options; <br />\Drupal\Core\Url::fromRoute(\'@routeName\', $routeParameters, $options);</div>', array(
          '@routeName' => $urlRoute->getRouteName(),
          '@parameters' => var_export($urlRoute->getRouteParameters(), TRUE),
          '@options' => var_export($urlRoute->getOptions(), TRUE),
        ));
      }
    }

    $form['result'] = array(
      '#markup' => $message,
      '#weight' => 100,
      '#prefix' => '<div class="devtools-url-form-result-wrapper">',
      '#suffix' => '</div>',
    );

    return $form;
  }

  /**
   * #element_validate handler for the "path" element in settingsForm().
   */
  public function validatePath(array $element, FormStateInterface $form_state) {
    $path = $form_state->getValue('path');

    if (empty($path)) {
      $form_state->setError($element, t('Path is empty'));
    }

    if (!preg_match('/^\/.*$/', $form_state->getValue('path'))) {
      $form_state->setError($element, t('Path must started /'));
    }
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  public function ajaxUrl(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('.devtools-url-form-result-wrapper', $form['result']));
    return $response;
  }
}