<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 8/9/15
 * Time: 2:12 AM
 */

namespace Drupal\devtools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class ModulesForm extends FormBase {
  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'devtools_modules_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached'] = array('library' => array('devtools/devtools_modules'));

    $form['modules_vt'] = array(
      '#type' => 'vertical_tabs',
    );

    if ($modules = \Drupal::moduleHandler()->getModuleList()) {
      foreach ($modules as $module) {

        $form[$module->getName()] = array(
          '#type' => 'details',
          '#title' => $module->getName(),
          '#description' => $module->getType(),
          '#group' => 'modules_vt',
        );

        $form[$module->getName()]['general'] = array(
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => 'General information',
          '#description' => '',
        );

        $form[$module->getName()]['general']['info'] = array(
          '#theme' => 'item_list',
          '#title' => format_string('$module = \Drupal::moduleHandler()->getModule(\'@name\');', array('@name' => $module->getName())),
          '#items' => array(

            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Machine name',
              '#example' => '(string) $module->getName()',
              '#value' => $module->getName()
            ),
            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Info file name',
              '#example' => '(string) $module->getFileName()',
              '#value' => $module->getFilename()
            ),
            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Extension file name',
              '#example' => '(string) $module->getExtensionFilename()',
              '#value' => $module->getExtensionFilename()
            ),
            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Extension path name',
              '#example' => '(string) $module->getExtensionPathname()',
              '#value' => $module->getExtensionPathname()
            ),
            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Path',
              '#example' => '(string) $module->getPath()',
              '#value' => $module->getPath()
            ),
            array(
              '#theme' => 'devtools_module_information',
              '#title' => 'Path name',
              '#example' => '(string) $module->getPathname()',
              '#value' => $module->getPathname()
            ),

          )

        );


        \Drupal::moduleHandler()->loadAllIncludes('install');
        if (function_exists($module->getName() . '_schema')) {

          $form[$module->getName()]['schema'] = array(
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => 'Schema information',
            '#description' => '',
          );

          $schemas = call_user_func($module->getName() . '_schema');

          foreach ($schemas as $name => $data) {


            $form[$module->getName()]['schema'][$name]['markup'] = array(
              '#prefix' => '<div><h3>' . $name . '</h3>',
              '#suffix' => '</div>',
              '#markup' => '<pre class="messages messages--status">' . var_export($data, TRUE) . '</pre>',
            );
          }

        }


      }
    }

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
