<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 11/08/15
 * Time: 11:06
 */

namespace Drupal\devtools\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devtools\CodeGen;
use Drupal\devtools\CodeGenDefinition;

class CodeGeneratorContext extends FormBase {
  public function getFormId() {
    return 'devtools_code_generator_context_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attributes']['class'] = ['devtools-code-generator--context-form-wrapper'];

    $args = $form_state->getBuildInfo()['args'];
    $context = (sizeof($args) > 0) ? $args[0] : CodeGenDefinition::CONTEXT_NONE;


    if ($choose = $form_state->getValue('context')) {
      if ($generatorId = \Drupal::routeMatch()->getParameter('name')) {
        CodeGen::setStorageContext($generatorId, $context, $choose);
      }
    }

    if ($context != CodeGenDefinition::CONTEXT_NONE) {

      $options = [];

      switch ($context) {
        case CodeGenDefinition::CONTEXT_MODULE:
          foreach (\Drupal::moduleHandler()
                     ->getModuleList() as $name => $module) {
            if ($module->getType() == 'module') {
              $options[$name] = $name;
            }
          }
          break;
        case CodeGenDefinition::CONTEXT_THEME:
          break;
      }



    }

    $form['context'] = [
      '#type' => 'select',
      '#title' => 'Context',
      '#required' => (sizeof($options) > 0) ? TRUE : FALSE,
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback']
      ]
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Set context'),
      '#button_type' => 'primary',
      '#attributes' => [
        'id' => 'code-gen--set-context-button'
      ],
    ];

//          dsm(CodeGen::getStorageContext('block', $context));

    return $form;
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.devtools-code-generator--context-form-wrapper', $form));
    return $response;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }
}