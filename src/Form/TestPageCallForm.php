<?php

namespace Drupal\devtools\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devtools\TestPage;

/**
 * Class TestPageCallForm.
 *
 * @package Drupal\devtools\Form
 */
class TestPageCallForm extends FormBase {

  const TEST_PAGE_CONFIG = 'devtools.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devtools_test_page_call_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $id = $this->getRequest()->get('id');

    if ($test = TestPage::getDefinitionById($id)) {
      $form['#prefix'] = '<div id="test-page-page-call-wrapper">';
      $form['#suffix'] = '</div>';
      $config = $this->config(self::TEST_PAGE_CONFIG);

      $default = (count($form_state->getValues())) ? $form_state->getValue('default') : $config->get('test_page');

      $form['default'] = [
        '#type' => 'value',
        '#value' => $default,
      ];

      $time_before = microtime();
      $test->pageCallback();
      $time_after = microtime();
      $request_time = number_format(($time_after - $time_before) * 1000, 3);

      $form['time'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="messages messages--status">{{ label }}: <strong>{{ time }} ms</strong></div>',
        '#context' => [
          'time' => $request_time,
          'label' => $this->t('Request callback time'),
        ],
      ];
      $form['result'] = ['#type' => 'container', '#weight' => 100,];
      if ($this->currentUser()->hasPermission($test->getPermission())) {


        $form['result'] += $test->getRenderResult();
      }else {
        $form['result'][] = [
          '#markup' => $this->t('Access denied for this content'),
        ];
      }



      $form['control'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['control-wrapper', 'clearfix']
        ],
      ];

      $form['control']['actions'] = [
        '#type' => 'actions',
        '#weight' => -100,
        '#attributes' => [
          'style' => '',
        ],
      ];

      $form['control']['actions']['submit'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Reload page'),
      ];

      if ($default['ajax']) {
        $form['control']['actions']['submit']['#ajax'] = [
          'wrapper' => 'test-page-page-call-wrapper',
        ];
      }

      $form['control']['options'] = [
        '#type' => 'container',
      ];

      $form['control']['options']['ajax'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use ajax for reload page'),
        '#default_value' => $default['ajax'],
      ];

    }
    else {
      drupal_set_message($this->t('Test page "@id" not found', ['@id' => $id]), 'error');
      return $form;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $default = [
      'ajax' => $form_state->getValue('ajax'),
    ];
    $form_state->setValue('default', $default);
  }

}
