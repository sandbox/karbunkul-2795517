<?php

namespace Drupal\devtools;

use Drupal\devtools\Plugin\CodeBenchmarkBase;

/**
 * Class CodeBenchmark.
 *
 * @package Drupal\devtools
 */
class CodeBenchmark {

  protected $benchmark;

  const BENCHMARK_CONFIG_NAME = 'devtools.code_benchmark';

  /**
   * CodeBenchmark constructor.
   *
   * @param string|CodeBenchmarkBase $id
   *   Benchmark id.
   * @param int $number_of_passes
   *   Number of passes benchmark.
   */
  public function __construct($id, $number_of_passes = 5) {
    $benchmark = (($id instanceof CodeBenchmarkBase)) ? $id : self::getBenchmarkById($id);

    if ($benchmark) {
      $this->setBenchmark($benchmark);
      $operations = array_fill(0, $number_of_passes, [[$this, 'processItem'], []]);
      $batch = [
        'title' => $benchmark->getLabel(),
        'operations' => $operations,
        'finished' => [$this, 'finished'],
      ];
      batch_set($batch);
    }
  }

  public static function newInstance($id, $number_of_passes = 5) {
    return new self($id, $number_of_passes);
  }

  /**
   * Batch process item.
   *
   * @param $context
   */
  public function processItem(&$context) {
    $pass = count($context['results']) + 1;
    $results = [];
    $benchmark = $this->getBenchmark();
    foreach ($benchmark->getBenchmarkCallback() as $callback) {
      $time_before = microtime();
      call_user_func([$benchmark, $callback]);
      $time_after = microtime();
      $results[$callback] = round(($time_after - $time_before) * 1000, 5);
    }
    $context['results'][] = $results;
    $context['message'] = $benchmark->getLabel() . ' #' . $pass;
  }

  /**
   * Call batch_process.
   */
  public function processBatch() {
    batch_process();
  }

  /**
   * Batch finished callback.
   *
   * @param $success
   * @param $results
   * @param $operations
   */
  public function finished($success, $results, $operations) {
    if ($success) {
      $total_results = [];
      foreach ($results as $key => $result) {
        foreach ($result as $name => $time) {
          $total_results[$name]['times'][] = $time;
        }
      }

      $averages = [];
      foreach ($total_results as $key => $arr) {
        $times = $arr['times'];
        $average = array_sum($times) / count($times);
        $averages[$key] = $average;
        $total_results[$key]['average'] = $average;
      }

      $best_fun = array_keys($averages, min($averages));
      $best_fun = reset($best_fun);
      $best_time = $total_results[$best_fun]['average'];

      $results = [];
      $results['results'] = $total_results;
      $results['function'] = $best_fun;
      $results['average'] = $best_time;

      $config = \Drupal::configFactory()->getEditable(self::BENCHMARK_CONFIG_NAME);
      $config->set($this->getBenchmark()->getPluginId(), $results)->save();
    }
  }

  /**
   * Get benchmark instance by id.
   *
   * @param string $id
   *   Benchmark id.
   *
   * @return bool|\Drupal\devtools\Plugin\CodeBenchmarkBase
   *   Benchmark instance.
   */
  public static function getBenchmarkById($id) {
    try {
      /** @var \Drupal\devtools\Plugin\CodeBenchmarkManager $manager */
      $manager = \Drupal::service('plugin.manager.devtools.code_benchmark');
      $instance = $manager->createInstance($id);
      return $instance;
    } catch (\Exception $e) {
      return FALSE;
    }
  }

  public static function getResultById($id) {
    if ($benchmark = self::getBenchmarkById($id)) {
      $result = \Drupal::config(self::BENCHMARK_CONFIG_NAME)->get($id);
      return (!is_null($result)) ? $result : FALSE;
    }

    return FALSE;
  }

  /**
   * @return CodeBenchmarkBase
   */
  private function getBenchmark() {
    return $this->benchmark;
  }

  /**
   * @param CodeBenchmarkBase $benchmark
   */
  private function setBenchmark(CodeBenchmarkBase $benchmark) {
    $this->benchmark = $benchmark;
  }

}
