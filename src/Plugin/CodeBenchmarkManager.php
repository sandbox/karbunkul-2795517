<?php

/**
 * @file
 * Contains \Drupal\devtools\Plugin\CodeBenchmarkPluginManager.
 */

namespace Drupal\devtools\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Code benchmark plugin plugin manager.
 */
class CodeBenchmarkManager extends DefaultPluginManager {

  /**
   * Constructor for CodeBenchmarkPluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/CodeBenchmark', $namespaces, $module_handler, 'Drupal\devtools\Plugin\CodeBenchmarkInterface', 'Drupal\devtools\Annotation\CodeBenchmark');

    $this->alterInfo('devtools_code_benchmark_info');
    $this->setCacheBackend($cache_backend, 'devtools_code_benchmark_plugins');
  }

}
