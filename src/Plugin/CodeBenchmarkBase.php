<?php

namespace Drupal\devtools\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Code benchmark plugin plugins.
 */
abstract class CodeBenchmarkBase extends PluginBase implements CodeBenchmarkInterface {

  /**
   * Get benchmark label.
   *
   * @return string
   *   Benchmark label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get benchmark provider.
   *
   * @return string
   *   Benchmark provider.
   */
  public function getProvider() {
    return $this->pluginDefinition['provider'];
  }

  /**
   * Get benchmark comment.
   *
   * @return string
   *   Benchmark comment.
   */
  public function getComment() {
    return '';
  }

}
