<?php

namespace Drupal\devtools\Plugin\CodeBenchmark;

use Drupal\devtools\Plugin\CodeBenchmarkBase;

/**
 * Class TestBenchmark.
 *
 * @CodeBenchmark(
 *   id = "devtools_test",
 *   label = @Translation("Test for code benchmark"),
 * )
 *
 * @package Drupal\devtools\Plugin\CodeBenchmark
 */
class DevtoolsTestBenchmark extends CodeBenchmarkBase {

  /**
   * {@inheritdoc}
   */
  public function getBenchmarkCallback() {
    return [
      'funOne',
      'funTwo',
    ];
  }

  public function funOne() {
    if ((2 * 2) == 5) {
      return TRUE;
   }
    else {
      return FALSE;
    }
  }

  public function funTwo() {
    return ((2 * 2) == 5) ? TRUE : FALSE;
  }

}
