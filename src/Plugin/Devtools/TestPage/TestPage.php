<?php

namespace Drupal\devtools\Plugin\Devtools\TestPage;

use Drupal\devtools\Plugin\TestPageBase;
use Drupal\devtools\TestPageItem;

/**
 * Class TestPage
 *
 * @DevtoolsTestPage(
 *   id = "devtools_test",
 *   label = @Translation("Devtools test"),
 * )
 *
 * @package Drupal\devtools\Plugin\Devtools\TestPage
 */
class TestPage extends TestPageBase {

  public function pageCallback() {
    $this->add(TestPageItem::newInstance($_GET)->setLabel('$_GET array'));
  }

}
