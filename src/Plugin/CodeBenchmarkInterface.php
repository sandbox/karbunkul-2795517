<?php

namespace Drupal\devtools\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Code benchmark plugin plugins.
 */
interface CodeBenchmarkInterface extends PluginInspectionInterface {

  /**
   * Get benchmark callback.
   *
   * @return array
   *   Benchmark callback(s).
   */
  public function getBenchmarkCallback();

}
