<?php

namespace Drupal\devtools\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\devtools\TestPage;

/**
 * Provides a 'TestPageBlock' block.
 *
 * @Block(
 *  id = "test_page_block",
 *  admin_label = @Translation("Test page callbacks"),
 * )
 */
class TestPageBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $items = [];
    foreach (TestPage::getDefinitions() as $definition) {
      $items[] = [
        '#type' => 'link',
        '#url' => Url::fromRoute('devtools.test_page', ['id' => $definition->getId()]),
        '#title' => $definition->getLabel(),
      ];
    }

    $build['test_page_block'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

}
