<?php

namespace Drupal\devtools\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Devtools test page plugins.
 */
interface TestPageInterface extends PluginInspectionInterface {

  public function pageCallback();

}

