<?php

namespace Drupal\devtools\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Devtools test page plugin manager.
 */
class TestPageManager extends DefaultPluginManager {

  /**
   * Constructor for DevtoolsTestPageManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Devtools/TestPage', $namespaces, $module_handler, 'Drupal\devtools\Plugin\TestPageInterface', 'Drupal\devtools\Annotation\DevtoolsTestPage');

    $this->alterInfo('devtools_test_page_info');
    $this->setCacheBackend($cache_backend, 'devtools_test_page_plugins');
  }

}
