<?php

namespace Drupal\devtools\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\devtools\TestPageItem;

/**
 * Base class for Devtools test page plugins.
 */
abstract class TestPageBase extends PluginBase implements TestPageInterface {

  protected $items = [];

  /**
   * @param \Drupal\devtools\TestPageItem $item
   */
  public function add(TestPageItem $item ) {
    $this->items[] = $item;
  }

  /**
   * @return TestPageItem[]
   */
  private function getItems() {
    return $this->items;
  }

  public function getRenderResult() {
    $build = [];

    foreach ($this->getItems() as $key => $item) {
      $title = ($item->getLabel() !== '') ? $item->getLabel() : '#' . $key;
      $title .= ' count: ' . count($item->getValue());
      $build[$key] = [
        '#type' => 'details',
        '#title' => $title,
      ];
      $build[$key][] = $item->getRender();
    }

    return $build;
  }

  public function getPermission() {
    return 'access content';
  }

  /**
   * Get benchmark label.
   *
   * @return string
   *   Benchmark label.
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * Get benchmark label.
   *
   * @return string
   *   Benchmark label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get benchmark provider.
   *
   * @return string
   *   Benchmark provider.
   */
  public function getProvider() {
    return $this->pluginDefinition['provider'];
  }

  /**
   * Get benchmark comment.
   *
   * @return string
   *   Benchmark comment.
   */
  public function getComment() {
    return '';
  }

}
