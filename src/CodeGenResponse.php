<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 22/08/15
 * Time: 03:52
 */

namespace Drupal\devtools;

class CodeGenResponse {
  protected $generatorID;
  protected $context;
  protected $generatorType;

  /**
   * @return mixed
   */
  public function getContext() {
    return CodeGen::getStorageContext($this->getGeneratorID(), $this->getGeneratorType());
  }

  /**
   * @return mixed
   */
  public function getGeneratorID() {
    return $this->generatorID;
  }

  /**
   * @param mixed $generatorID
   */
  public function setGeneratorID($generatorID) {
    $this->generatorID = $generatorID;
  }

  /**
   * @return mixed
   */
  public function getGeneratorType() {
    return $this->generatorType;
  }

  /**
   * @param mixed $generatorType
   */
  public function setGeneratorType($generatorType) {
    $this->generatorType = $generatorType;
  }


}