<?php

namespace Drupal\devtools;

use Drupal\devtools\Plugin\TestPageBase;
use Drupal\devtools\Plugin\TestPageManager;

/**
 * Class TestPage.
 *
 * @package Drupal\devtools
 */
class TestPage {

  /**
   * @return TestPageBase[]
   */
  public static function getDefinitions() {
    $definitions = [];
    /** @var TestPageManager $manager */
    $manager = \Drupal::service('plugin.manager.devtools.test_page');
    foreach ($manager->getDefinitions() as $definition) {
      $instance = $manager->createInstance($definition['id']);

      if ($instance instanceof TestPageBase) {
        $definitions[$instance->getId()] = $instance;
      }
    }
    return $definitions;
  }

  /**
   * @param $definition_id
   *
   * @return bool|TestPageBase
   */
  public static function getDefinitionById($definition_id) {
    $definitions = self::getDefinitions();
    return (isset($definitions[$definition_id])) ? $definitions[$definition_id] : FALSE;
  }

}