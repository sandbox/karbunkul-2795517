<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 16/08/15
 * Time: 02:47
 */

namespace Drupal\devtools;

use Drupal\Core\Url;

class CodeGenDefinition {
  const CONTEXT_NONE = 0;
  const CONTEXT_MODULE = 1;
  const CONTEXT_THEME = 2;

  protected $id = NULL;
  protected $title = NULL;
  protected $description = NULL;
  protected $context = self::CONTEXT_NONE;
  protected $form = NULL;



  public function getLink($title) {
    return \Drupal::l($title, $this->getUrl());
  }

  public function getUrl() {
    $path = str_replace('_', '-', $this->getId());
    return Url::fromRoute('devtools.codegen_page', ['name' => $path]);
  }

  /**
   * @return null
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param null $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  public function isValidate() {

    $requiredData = [
      $this->getTitle(),
      $this->getId(),
      $this->getDescription(),
      $this->getForm(),
    ];

    foreach ($requiredData as $property) {
      if (is_null($property)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * @return null
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param null $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return null
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param null $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return null
   */
  public function getForm() {
    return $this->form;
  }

  /**
   * @param null $form
   */
  public function setForm($form) {
    $this->form = $form;
  }

  /**
   * @return null
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * @param null $context
   */
  public function setContext($context) {
    $this->context = $context;
  }

}