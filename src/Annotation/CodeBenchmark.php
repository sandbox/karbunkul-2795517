<?php

namespace Drupal\devtools\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Code benchmark plugin item annotation object.
 *
 * @see \Drupal\devtools\Plugin\CodeBenchmarkManager
 * @see plugin_api
 *
 * @Annotation
 */
class CodeBenchmark extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
