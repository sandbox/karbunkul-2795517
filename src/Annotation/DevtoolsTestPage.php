<?php

/**
 * @file
 * Contains \Drupal\devtools\Annotation\DevtoolsTestPage.
 */

namespace Drupal\devtools\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Devtools test page item annotation object.
 *
 * @see \Drupal\devtools\Plugin\TestPageManager
 * @see plugin_api
 *
 * @Annotation
 */
class DevtoolsTestPage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
