<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 8/9/15
 * Time: 12:01 AM
 */

namespace Drupal\devtools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\devtools\CodeGen;
use Drupal\devtools\CodeGenDefinition;

class CodeGeneratorController extends ControllerBase {

  public function mainPage() {
    $build = [];

    foreach (array_keys(CodeGen::getGenerators()) as $id) {
      if ($generator = CodeGen::getGenerator($id)) {

        $build[$generator->getId()] = [
          '#theme' => 'devtools_generator_information',
          '#title' => $generator->getTitle(),
          '#description' => $generator->getDescription(),
          '#link' => $generator->getLink('more'),
        ];

      }

    }

    $build['#attached']['library'] = 'devtools/devtools.code_generator.global';

    return $build;
  }

  public function generator($name) {

    $generator = CodeGen::getGenerator(str_replace('-', '_', $name));

    $build = [];

    $build['#attached']['library'] = 'devtools/devtools.code_generator.global';

    $build['description'] = ['#markup' => $generator->getDescription()];
    $context = $generator->getContext();
    if ($context != CodeGenDefinition::CONTEXT_NONE) {
      $build['context'] = \Drupal::formBuilder()
        ->getForm('\Drupal\devtools\Form\CodeGeneratorContext', $context);
    }

    $build['generator'] = \Drupal::formBuilder()
      ->getForm($generator->getForm());

//    dsm($generator);

    return $build;
  }

  public function generatorTitle($name) {
    $generator = CodeGen::getGenerator(str_replace('-', '_', $name));
    return $generator->getTitle();
  }

}