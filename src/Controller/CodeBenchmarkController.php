<?php

namespace Drupal\devtools\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\devtools\CodeBenchmark;
use Drupal\devtools\Plugin\CodeBenchmarkBase;

/**
 * Class CodeBenchmarkController.
 *
 * @package Drupal\devtools\Controller
 */
class CodeBenchmarkController extends ControllerBase {

  /**
   * Benchmark list callback.
   *
   * @return string
   *   Return Hello string.
   */
  public function benchmarkList() {
    /** @var \Drupal\devtools\Plugin\CodeBenchmarkManager $manager */
    $manager = \Drupal::service('plugin.manager.devtools.code_benchmark');

    /** @var CodeBenchmarkBase[] $benchmarks */
    $benchmarks = [];
    $rows = [];
    foreach ($manager->getDefinitions() as $definition) {
      $benchmark = $manager->createInstance($definition['id']);
      if ($benchmark instanceof CodeBenchmarkBase) {
        $benchmarks[] = $benchmark;
        $id = $benchmark->getPluginId();

        $result = CodeBenchmark::getResultById($id);
        $result_str = ($result) ? new FormattableMarkup('@function (@average)',
          [
            '@function' => $result['function'],
            '@average' => number_format($result['average'], 5),
          ]) : '';

        $link = Link::createFromRoute($benchmark->getLabel(),
          'devtools.code_benchmark_call',
          ['id' => $id]);
        $rows[] = [
          'id' => $link,
          'module' => $benchmark->getProvider(),
          'result' => $result_str,
        ];
      }
    }

    return [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Benchmark'),
        'module' => $this->t('Module'),
        'result' => $this->t('Result'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('Benchmark not found.'),
    ];
  }

}
