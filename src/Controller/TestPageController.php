<?php

namespace Drupal\devtools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\devtools\Plugin\TestPageBase;
use Drupal\devtools\TestPage;

/**
 * Class CodeBenchmarkController.
 *
 * @package Drupal\devtools\Controller
 */
class TestPageController extends ControllerBase {

  /**
   * Benchmark list callback.
   *
   * @return string
   *   Return Hello string.
   */
  public function testListCallback() {
    /** @var \Drupal\devtools\Plugin\TestPageManager $manager */
    $manager = \Drupal::service('plugin.manager.devtools.test_page');

    /** @var TestPageBase[] $pages */
    $pages = [];
    $rows = [];
    foreach ($manager->getDefinitions() as $definition) {
      $page = $manager->createInstance($definition['id']);
      if ($page instanceof TestPageBase) {
        $pages[] = $page;
        $id = $page->getPluginId();

        $link = Link::createFromRoute($page->getLabel(),
          'devtools.test_page',
          ['id' => $id]);
        $rows[] = [
          'id' => $link,
          'module' => $page->getProvider(),
        ];
      }
    }

    return [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Label'),
        'module' => $this->t('Module'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('Test page not found.'),
    ];
  }

  public function testTitleCallback($id) {
    $callback = TestPage::getDefinitionById($id)->getLabel();
    return $callback;
  }

}
