<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 8/9/15
 * Time: 12:01 AM
 */

namespace Drupal\devtools\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

class DevtoolsHelperController extends ControllerBase {
  public function flushCaches() {
    drupal_flush_all_caches();
    drupal_set_message(t('Cache cleared.'));

    if ($refer = \Drupal::request()->headers->get('referer')) {
      $refer = str_replace(\Drupal::request()
        ->getSchemeAndHttpHost(), '', $refer);

      if ($refer != \Drupal::request()->getRequestUri()) {
        $route = Url::fromUserInput($refer);
        return $this->redirect($route->getRouteName(), $route->getRouteParameters(), $route->getOptions());
      }
    }

    return array();
  }

  public function formBuildInfo($js) {
    if ($js == 'ajax') {
      $response = new AjaxResponse();
      $response->addCommand(new InvokeCommand('html', 'toggleClass', ['form-build-info']));

      $config = \Drupal::configFactory()
        ->getEditable('devtools.settings');
      $config->set('show_form_buid_info', !$config->get('show_form_buid_info'))
        ->save();
      return $response;
    }
    else {
      return array();
    }
  }

}