<?php
/**
 * Created by PhpStorm.
 * User: karbunkul
 * Date: 8/9/15
 * Time: 12:01 AM
 */

namespace Drupal\devtools\Controller;

use Drupal\Core\Controller\ControllerBase;

class DevtoolsModulesController extends ControllerBase {
  public function main() {

    drupal_flush_all_caches();

    // Все ключи конфигов \Drupal::configFactory()->listAll();

    $rendered = array();
    $rendered['form'] = \Drupal::formBuilder()
      ->getForm('Drupal\devtools\Form\ModulesForm');
    return $rendered;
  }

}