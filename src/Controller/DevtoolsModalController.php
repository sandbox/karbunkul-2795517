<?php

namespace Drupal\devtools\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

class DevtoolsModalController extends ControllerBase {
  public function pathToRoute($js) {
    $form = \Drupal::formBuilder()
      ->getForm('Drupal\devtools\Form\PathToRouteForm');

    if ($js == 'ajax') {
      $response = new AjaxResponse();
      $response->addCommand(
        new OpenModalDialogCommand(t('Path to Route'), render($form),
          array(
            'width' => 800,
            'closeOnEscape' => FALSE
          ,
          ))
      );
      return $response;
    }
    else {
      return $this->redirect('devtools.url');
    }
  }

}