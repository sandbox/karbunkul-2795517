<?php

namespace Drupal\devtools;

/**
 * Class TestPageItem.
 *
 * @package Drupal\devtools
 */
class TestPageItem {

  protected $label;
  protected $type;
  protected $value;

  /**
   * TestPageItem constructor.
   *
   * @param string $value
   * @param string $type
   */
  public function __construct($value, $type = 'dump') {
    $this->type = $type;
    $this->value = $value;
    $this->setLabel('');
  }

  /**
   * @param string $value
   * @param string $type
   *
   * @return $this
   */
  public static function newInstance($value, $type = 'dump') {
    return new self($value, $type);
  }

  /**
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * @param string $label
   *
   * @return $this
   */
  public function setLabel($label) {
    $this->label = $label;
    return $this;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   *
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;
    return $this;
  }

  /**
   * @return string
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * @param string $value
   *
   * @return $this
   */
  public function setValue($value) {
    $this->value = $value;
    return $this;
  }

  public function getRender() {
    switch ($this->getType()) {
      case 'dump':
        $dump = print_r($this->getValue(), true);
        return [
          '#type' => 'inline_template',
          '#template' => '<code><pre>{{ dump }}</pre></code>',
          '#context' => ['dump' => $dump],
        ];
        break;
    }
  }

}
