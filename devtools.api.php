<?php

function hook_code_generator() {
  $items = [];

  $items['block'] = [
    'title' => 'Generate block',
    'form' => 'Drupal\devtools\Form\CodeGeneratorForm',
    'description' => 'Generate new block',
  ];

  return $items;
}
